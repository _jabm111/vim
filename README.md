# Vim setup

This repos uses submodules to manage vim plugins. Supports Vim v8+. As of Vim 8 you can include packages without a package manager by placing the package repos in the `pack/*/start/` or `pack/*/opt/` (optional - need to be manually loaded) directories. Also, Vim 8 added async functionality so packages can be much more performant (linting, git gutter, etc).

To pull the repo and all submodules:

`git clone --recursive <this-repo>`

## Creating symlinks
If you want to create symlinks (WARNING: it will override your files if they already exist):

NOTE: use absolute paths for the commands below otherwise you may have issues using the symlinks

To link this repo so Vim will load the plugins:

`ln -sf <this-repo> ~/.vim`

To link the config files in this repo:

`ln -sf <this-repo>/.vimrc ~/.vimrc`
`ln -sf <this-repo>/.zshrc ~/.zshrc`
`ln -sf <this-repo>/.ackrc ~/.ackrc`

Got the idea from this gist:

https://gist.github.com/manasthakur/d4dc9a610884c60d944a4dd97f0b3560
