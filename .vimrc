packadd! onedark.vim
set nocompatible
let g:ctrlp_max_files=0
let g:ale_linters = {'javascript': ['eslint']}
let g:ale_lint_on_text_changed = 'never'
let g:jsx_ext_required = 0
let g:onedark_termcolors = 256
syntax on
syntax enable
filetype plugin indent on
set t_Co=256
set background=dark
let mapleader = ","
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set encoding=utf-8
set autoindent
set number
"set relativenumber
"Minimum offset from cursor to top/bottom of screen
set scrolloff=5
set showmode
set showcmd
"Allow buffers to be hidden in the background without saving changes
"set hidden
"let loaded_matchparen = 1
"Allow for autocompletion in command line
set wildmenu
set wildmode=list:longest
set wildignore+=*/public/*,*/node_modules/*,*/puphpet/*,*/vendor/*,*/bower_components/*,*.swp,*.un
"Visual flash instead of beep
set visualbell
"Helps with smooth scrolling
set ttyfast
"Show line and column numbers in the status bar
set ruler
"Set expected backspace behaviour for a text editor
set backspace=indent,eol,start
"Status is always 2 lines at the bottom
set laststatus=2
"Following two combined make searches case insensitive unless you include an
"uppercase character in your search string
set ignorecase
set smartcase
"Global default for search and replace (and maybe other things)
set gdefault
"Search as you type after /
set incsearch
"Highlight all search matches
set hlsearch
"Wrap lines
set wrap
set formatoptions=qrn1
"au FocusLost * :wa
"Switch between buffers
nnoremap <C-n> :bnext<CR>
nnoremap <C-h> :bprevious<CR>
"Search from current directory, excluding unnecessary folders
nnoremap <leader>f :grep -i -r --exclude-dir=Libraries --exclude-dir=.git --exclude-dir=ios --exclude-dir=android --exclude-dir=node_modules "" .<left><left><left>
"Explore file tree
nnoremap <leader>n :Explore<CR>
"Strip file of trailing whitespace
nnoremap <leader>w :%s/\s\+$//<cr>:let @/=''<CR>
"Clear last search
nnoremap <leader><space> :noh<cr>
"copy to clipboard
vnoremap <leader>c :w !pbcopy<CR><CR>
colorscheme onedark
